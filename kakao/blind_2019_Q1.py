def LRE(s, n=1):
    i = 0
    target = s[i:i+n]
    cnt = 0
    result = ''
    result_len = len(s)
    
    while i < len(s):
        if s[i:i+n] != target:
            if cnt == 1:
                result += target
            else:
                result += str(cnt) + target
            cnt = 1
            target = s[i:i+n]
        else:
            cnt += 1
        i = i+n


    if cnt == 1:
        result += target
    else:
        result += str(cnt) + target
    return result

def solution(s):
    slen = len(s)
    min_len = slen
    print(f'원래 길이 : {slen}')
    for i in range(1, slen+1):
        encoding_result = LRE(s, i)
        encoding_length = len(encoding_result)
        if encoding_length < 1:
            encoding_length = slen
        if encoding_length < min_len:
            min_len = encoding_length
        print(f'{encoding_result}를 {i}로 잘라 인코딩 = {encoding_length}')
    return min_len

# print(solution('aaaaaaaaaa'))
# print(solution('xababcdcdababcdcd'))
# print(solution('abcabcabcabcdededededede'))
# print(solution('abcabcdede'))
# print(solution('aabbaccc'))
# print(solution('ababcdcdababcdcd'))
