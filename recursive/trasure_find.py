# 가장 많은 보물 찾기
# 1행 1열에서 출발하며 오른쪽, 아래쪽으로만 이동 가능하다
N = 5
tmap = [[0, 1, 0, 0, 1],
        [0, 0, 1, 0, 0],
        [1, 0, 1, 1, 0],
        [1, 1, 0, 1, 0],
        [1, 0, 0, 0, 1]]

max_t = 0
total_route_case = 0
def find_max_trasure(r, c, max_tmp, route):
    global max_t, total_route_case
    
    # 목표에 도달하면 종료한다.
    if r is N-1 and c is N-1:
        # Add destination score
        max_tmp += tmap[r][c]
        total_route_case += 1
        
        # 최대점수를 갱신한다
        if max_t < max_tmp:
            max_t = max_tmp

        print(f'trasure={max_tmp} : route={route}')
        return max_t

    route.append((r, c))

    # 맵을 벗어나지 않도록 한다.
    if r < N-1: # 아래쪽부터 검색 후
        find_max_trasure(r+1, c, max_tmp + tmap[r][c], route)
        
    if c < N-1: # 오른쪽 검색
        find_max_trasure(r, c+1, max_tmp + tmap[r][c], route)

    route.pop()

find_max_trasure(0, 0, 0, [])
print(f'maximum trasure : {max_t}')
print(f'total route case : {total_route_case}')
