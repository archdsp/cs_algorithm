recurse = 0
def solution(pay, spec=[0, 0, 0], idx=0, tp=[10, 5, 1]):
    global recurse
        
    if pay is 0:
        print(spec)
        recurse += 1
        spec[idx-1] = 0
        return

    if idx is len(tp)-1:
        spec[idx] = pay
        print(f'ans = {spec}')
        spec[idx] = 0
        return

    for k in range(idx, len(tp)):
        if pay < tp[k]:
            continue
        q, r = divmod(pay,tp[k])
        for i in range(1, q+1):
            spec[k] = i
            print(f'{pay} - {i*tp[k]}를 {idx+1}로 구성하는 문제 {spec}, {idx}, {i}, {k}')
            solution(pay - i*tp[k], spec, idx+1)
        
# 동전으로 값 지불하기 : 여러 종류로 돈 지불하기
if __name__ == '__main__':
    # 지불해야할 값
    pay = 10
    solution(pay)
