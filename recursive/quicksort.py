from matplotlib import pyplot as plt
from multipledispatch import dispatch

@dispatch(list)
def quicksort(x):
    if len(x) <= 1:
        return x

    pivot = x[len(x) // 2]
    less = []
    more = []
    equal = []
    for a in x:
        if a < pivot:
            less.append(a)
        elif a > pivot:
            more.append(a)
        else:
            equal.append(a)

    return quicksort(less) + equal + quicksort(more)

def _quicksort(arr, left_idx, right_idx):
    pivot = right_idx
    right_idx -= 1

    while True:
        while arr[left_idx] < arr[pivot]:
            left_idx += 1
            if left_idx == len(arr):
                return left_idx

        while right_idx >= 0 and arr[right_idx] > arr[pivot]:
            right_idx -= 1

        if left_idx >= right_idx:
            break

        tmp = arr[left_idx]
        arr[left_idx] = arr[right_idx]
        arr[right_idx] = tmp

    tmp = arr[left_idx]
    arr[left_idx] = arr[pivot]
    arr[pivot] = tmp

    return left_idx

@dispatch(list, int, int)
def quicksort(arr, left, right):
    if right - left < 1:
        return

    pivot = _quicksort(arr, left, right)
    quicksort(arr, left, pivot-1)
    quicksort(arr, pivot+1, right)

def test_quicksort(level=10):
    import random
    import time
    import numpy as np
    N = 1
    times = []
    number = []
    for i in range(0, level):
        arr = [random.randint(0, N) for i in range(0, N)]
        start = time.time()
        quicksort(arr)
        end = time.time()
        number.append(N)
        times.append(end-start)
        N <<= 1
    plt.plot(number, times)
    plt.plot(number, number * np.log(number)*times[0])
    plt.yscale('log')
    plt.xscale('log', basex=2)
    plt.show()
    print(times, number)

if __name__ == '__main__':
    test_quicksort()
    #arr = [1, 9, 5, 1, 9, 9, 4, 10, 5, 8, 1, 12, 14, 13, 14, 12]
    #arr = [3,3,3]
