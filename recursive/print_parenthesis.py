import math
from itertools import combinations

#문제 :  N이 주어졌을 때 알맞은 괄호 짝 조합 출력하기
def solution(ans, N, open_p, close_p):
    if len(ans) is N*2:
        print(ans)
        return
    if open_p < N:
        solution(ans+'(', N, open_p+1, close_p)

    if close_p < open_p:
        solution(ans+')', N, open_p, close_p+1)

solution('', 3, 0, 0)
