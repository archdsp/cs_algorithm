# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 02:03:12 2019

@author: dsparch
"""

import random


def quick_sort(arr, start, end):
    if start < end:
        pivot = partition(arr, start, end)
        quick_sort(arr, start, pivot - 1)
        quick_sort(arr, pivot + 1, end)
    return arr

def quick_select(arr, start, end, k):
    if start == end:
        return arr[start]
    pivot = partition(arr, start, end)
    if pivot > k:
        return quick_select(arr, start, pivot - 1, k)
    elif pivot < k:
        return quick_select(arr, pivot + 1, end, k)
    else:
        return arr[pivot]

def partition(l, left_idx, right_idx):
    left = left_idx
    right = right_idx -1
    pivot = right_idx
    
    while True:        
        while left <= right and l[left] <= l[pivot]:
            left += 1
        
        while left <= right and l[right] >= l[pivot]:
            right -= 1
            
        if left > right:
            break
        l[left], l[right] = l[right], l[left]
    l[left], l[pivot] = l[pivot], l[left]
    return left

#    
#def quickselect():
L = [random.randint(1, 20) for _ in range(0, 10)]
print(quick_select(L, 0, len(L)-1, 3))